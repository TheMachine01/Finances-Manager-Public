Um euch einen Einblick in die Finanz-Manager App zu verschaffen habe ich dieses Repo erstellt wodurch ihr die App testen könnt.
Die App ist noch in Entwicklung und nicht ausgreift, es gibt also noch Bugs und nicht alle Funktionen stehen zur Verfügung.
Ein paar kann jedoch bereits testen.
Diese wären

- Ausnahmen / Einnahmen eintragen 

- Diese in einer Listen/Historien Ansicht ansehen und filtern

- Dashboard mit verschiedenen Diagrammen um die eigene Statistik zu beobachten
  - Wie viel habe ich im Vergleich zu den letzten 3 Tagen oder Monaten ausgegeben ?
  - Für was gebe ich viel Geld aus ?



---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  

- [Webapp testen](#webapp-testen)
- [Mobileapp testen](#mobileapp-testen)



<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Webapp testen 

Um die Webapp-Version zu testen klicke auf diesen Link [Finance-Manager](https://finance-manager-17732.web.app/).
Bitte bachte das die App bis jetzt nur für Mobilgeräte ausgelegt ist und deshalb ein viewport-resulotion von 360x741 geeignet ist.

## Mobileapp testen 

Um die Mobil-Version der App zu testen, navigiere zum Mobile-Version Branch und lade dir die .apk Datei herunter.
Diese kannst du dann als normale Androidapp auf deinem Gerät testen.


